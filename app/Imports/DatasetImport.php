<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

use App\Model\Datasets;

class DatasetImport implements ToCollection
{
    private $namaBulan = [
        1   => 'Januari',
        2   => 'Februari',
        3   => 'Maret',
        4   => 'April',
        5   => 'Mei',
        6   => 'Juni',
        7   => 'Juli',
        8   => 'Agustus',
        9   => 'September',
        10  => 'Oktober',
        11  => 'November',
        12  => 'Desember'
    ];

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection) {    
        $collection->shift();

        $data = [];

        foreach($collection as $index => $dataset) {
            $data[$index]['bulan'] = $dataset[0] ?? null;
            $data[$index]['nama_bulan'] = $this->namaBulan[$dataset[0]] ?? null;
            $data[$index]['tahun'] = $dataset[1] ?? null;
            $data[$index]['jumlah'] = $dataset[2] ?? null;
            $data[$index]['created_at'] = now();
            $data[$index]['updated_at'] = now();
        }

        Datasets::insert($data);
    }
}
