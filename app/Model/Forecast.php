<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Forecast extends Model
{
    protected $table = 'forecasts';

    protected $fillable = [
        'pid',
        'bulan',
        'tahun',
        'prediksi'
    ];
}
