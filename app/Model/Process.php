<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = 'process';

    public function result() {
        return $this->hasMany('App\Model\Result', 'pid', 'id');
    }

    public function forecast() {
        return $this->hasMany('App\Model\Forecast', 'pid', 'id');
    }
}
