<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';
    protected $fillable = [
        'pid',
        'alpha',
        'bulan',
        'nama_bulan',
        'tahun',
        'jumlah',
        'st1',
        'st2',
        'at',
        'bt',
        'yt',
		'mape'
    ];
}
