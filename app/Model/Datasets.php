<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Datasets extends Model
{
    protected $table = 'datasets';
    protected $fillable = [
        'bulan',
        'nama_bulan',
        'tahun',
        'jumlah'
    ];
}
