<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\DatasetImport;
use App\Model\Datasets;
use App\Model\Result;
use App\Model\Process;
use App\Model\Forecast;
use Carbon\Carbon;

class DashboardController extends Controller
{
    private float $alpha = 0.1;

    public function index() {
        $datasets = Datasets::orderBy('tahun', 'asc')->orderBy('bulan', 'asc')->get();

        return view('my-dashboard', [
            'datasets' => $datasets
        ]);
    }

    public function import(Request $request) {
        try {
            if ($request->hasFile('file_excel')){
                Datasets::query()->truncate();

                $path = $request->file('file_excel')->getRealPath();
                \Excel::import(new DatasetImport, $request->file_excel);
            }

            return redirect()->back()->with('successMessage', 'Dataset berhasil di import');

        } catch(\Exception $e) {
            return redirect()->back()->with('errorMessage', $e->getMessage());
        } 
    }

    public function hitung(Request $request) {        
        $datasets = Datasets::orderBy('tahun', 'asc')->orderBy('bulan', 'asc')->get();

        Process::query()->truncate();
        Result::query()->truncate();
        Forecast::query()->truncate();
        
        $alphas = [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 ];

        foreach($alphas as $alpha) {
            $process = Process::create();
            $this->alpha = $alpha;

            $counter = 0;
            $recordTerakhir = null;
            $hasil = [];
            $lastAt = 0;
            $lastBt = 0;

            foreach($datasets as $dataset) {
                $hasil[$counter]['pid']        = $process->id;
                $hasil[$counter]['alpha']      = $alpha;
                $hasil[$counter]['bulan']      = $dataset->bulan;
                $hasil[$counter]['bulan']      = $dataset->bulan;
                $hasil[$counter]['nama_bulan'] = $dataset->nama_bulan;
                $hasil[$counter]['tahun']      = $dataset->tahun;
                $hasil[$counter]['jumlah']     = $dataset->jumlah;

                if ($counter == 0) {
                    $hasil[$counter]['st1'] = $dataset->jumlah;
                    $hasil[$counter]['st2'] = $dataset->jumlah;
                    $hasil[$counter]['at']  = $this->hitungAt($hasil[$counter]['st2'], $hasil[$counter]['st1']);     
                    $hasil[$counter]['bt']  = $this->hitungBt($hasil[$counter]['st2'], $hasil[$counter]['st1']);
                    $hasil[$counter]['yt']  = $dataset->jumlah;
                    $hasil[$counter]['mape']  = 0;
                    
                } else {
                    $hasil[$counter]['st1'] = $this->hitungSt1($hasil[$counter-1]['jumlah'], $hasil[$counter-1]['st1']);
                    $hasil[$counter]['st2'] = $this->hitungSt2($hasil[$counter]['st1'], $hasil[$counter-1]['st2']);
                    $hasil[$counter]['at']  = $this->hitungAt($hasil[$counter-1]['st2'], $hasil[$counter-1]['st1']);    
                    $hasil[$counter]['bt']  = $this->hitungBt($hasil[$counter-1]['st2'], $hasil[$counter-1]['st1']);
                    $hasil[$counter]['yt']  = $this->hitungYt($hasil[$counter]['at'], $hasil[$counter]['bt']);
                    $hasil[$counter]['mape']  = $this->hitungMape($hasil[$counter]['yt'], $hasil[$counter]['jumlah']);
                }

                $hasil[$counter]['created_at'] = now();
                $hasil[$counter]['updated_at'] = now();
                
                $lastAt = $hasil[$counter]['at'];
                $lastBt = $hasil[$counter]['bt'];
                
                $counter++;
            }

            Result::insert($hasil);

            $result = Result::where('pid', $process->id)->get();
            $lastRecord = Result::where('pid', $process->id)->orderBy('created_at')->first();
            $lastDate = Carbon::parse($lastRecord->tahun . '-' . $lastRecord->bulan . '-1');

            $monthName = [null, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            $resultNextMonth = [];
            for($i=1;$i<=4;$i++) {
                $nextMonth = $lastDate->copy()->addMonths($i);
                $resultNextMonth[$i]['pid'] = $process->id;
                $resultNextMonth[$i]['bulan'] = $monthName[$nextMonth->format('n')];
                $resultNextMonth[$i]['tahun'] = $nextMonth->format('Y');
                $resultNextMonth[$i]['prediksi'] = $this->hitungPrediksi($lastAt, $lastBt, $i);
            }

            Forecast::insert($resultNextMonth);
        }

        $process = Process::all();
        $charts = [];
        $charts['label'] = Result::selectRaw("CONCAT(nama_bulan, ' ', tahun) as 'date'")->groupBy(['nama_bulan', 'tahun'])->orderBy('id', 'asc')->get()->pluck('date')->toArray();
        $charts['data_aktual'] = Datasets::orderBy('id', 'asc')->get();
        $charts['data_total'] = [];

        foreach($process as $k => $v) {
            foreach($v->result as $result) {
                $charts['data_total'][$k][] = $result->yt;
            }

            $charts['data_total'][$k] = implode(',', $charts['data_total'][$k]);
        }

        logger(print_r($charts, true));

        return view('my-dashboard', [
            'datasets'  => $datasets,
            'processes' => $process,
            'charts'    => $charts
        ]);
    }


    public function hitungSt1($jumlah, $st1) {
        return ($this->alpha * $jumlah) + ((1-$this->alpha) * $st1);
    }

    public function hitungSt2($st1, $st2) {
        return ($this->alpha * $st1) + ((1-$this->alpha) * $st2);
    }

    public function hitungAt($st2, $st1) {
        return round(2 * $st2 - $st1,0);
    }

    public function hitungBt($st2, $st1) {
	return abs (round(($this->alpha / (1-$this->alpha)) * ($st2 - $st1),0));
    }

    public function hitungYt($at, $bt) {
        return ($at + ($bt * 1));
    }
		public function hitungMape($jumlah, $yt) {
        return abs((( $jumlah - $yt ) / $jumlah)) *100 ;
    }

	public function hitungPrediksi($at, $bt, $sequence) {
        return abs( ($at + ($bt  * $sequence) )) ;
    }
}
