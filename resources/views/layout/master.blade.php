<!DOCTYPE html>
<html>
<head>
  <title>Star Admin Pro Laravel Dashboard Template</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="{{ csrf_token() }}">
  
  <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

  <!-- plugin css -->
  {!! Html::style('assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
  {!! Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
  <!-- end plugin css -->

  @stack('plugin-styles')

  <!-- common css -->
  {!! Html::style('css/app.css') !!}
  <!-- end common css -->

  @stack('style')
</head>
<body data-base-url="{{url('/')}}">

  <div class="container-scroller" id="app">
    @include('layout.header')
    <div class="container-fluid page-body-wrapper">
      @include('layout.sidebar')
      <div class="main-panel">
        <div class="content-wrapper">

          @if (session()->has('successMessage'))
          <div class="toast" id="toast-sukses" style="position: absolute; top: 1; right: 20px; z-index: 9999" data-delay="3000">
            <div class="bg-success toast-body text-light">
                <strong>Sukses</strong><br/>
                {{ session()->get('successMessage') }}
            </div>
          </div>
          @endif

          @if (session()->has('errorMessage'))
          <div class="toast" id="toast-gagal" style="position: absolute; top: 1; right: 20px; z-index: 9999" data-delay="3000">
            <div class="bg-danger toast-body text-light">
                <strong>Kesalahan</strong><br/>
                {{ session()->get('errorMessage') }}
            </div>
          </div>
          @endif
          
          @yield('content')

        </div>
        @include('layout.footer')
      </div>
    </div>
  </div>

  <!-- base js -->
  {!! Html::script('js/app.js') !!}
  {!! Html::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
  <!-- end base js -->

  <!-- plugin js -->
  @stack('plugin-scripts')
  <!-- end plugin js -->

  <!-- common js -->
  {!! Html::script('assets/js/off-canvas.js') !!}
  {!! Html::script('assets/js/hoverable-collapse.js') !!}
  {!! Html::script('assets/js/misc.js') !!}
  {!! Html::script('assets/js/settings.js') !!}
  {!! Html::script('assets/js/todolist.js') !!}
  <!-- end common js -->

  @stack('custom-scripts')
  <script>
    $(document).ready(function(){
      @if (session()->has('successMessage'))
        $('#toast-sukses').toast('show');
      @endif

      @if (session()->has('errorMessage'))
        $('#toast-gagal').toast('show');
      @endif
    });
  </script>
</body>
</html>