@extends('layout.master')

@push('style')
<style>
    .form-control-sm { height: 2rem !important }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">                
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link {{ isset($processes) ? '' : 'active' }}" id="nav-import-tab" data-toggle="tab" href="#nav-import" role="tab" aria-controls="nav-import" aria-selected="true">Import Data</a>
                        <a class="nav-item nav-link" id="nav-table-tab" data-toggle="tab" href="#nav-table" role="tab" aria-controls="nav-table" aria-selected="false">Tabel Data</a>
                        <a class="nav-item nav-link {{ isset($processes) ? 'active' : '' }}" id="nav-perhitungan-tab" data-toggle="tab" href="#nav-perhitungan" role="tab" aria-controls="nav-perhitungan" aria-selected="false">Perhitungan</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade {{ isset($processes) ? '' : 'show active' }} m-3" id="nav-import" role="tabpanel" aria-labelledby="nav-import-tab">
                        <form method="post" action="{{ route('import') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="import-excel">Import Data : </label>
                                <input type="file" class="form-control-file" id="import-excel" name="file_excel">
                            </div>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </form>
                    </div>
                    <div class="tab-pane fade m-3" id="nav-table" role="tabpanel" aria-labelledby="nav-table-tab">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Bulan</th>
                                <th scope="col">Tahun</th>
                                <th scope="col">Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $number = 1; @endphp
                                @foreach($datasets as $dataset)
                                <tr>
                                    <td>{{ $number }}</td>
                                    <td>{{ $dataset->nama_bulan }}</td>
                                    <td>{{ $dataset->tahun }}</td>
                                    <td>{{ $dataset->jumlah }}</td>
                                </tr>
                                @php $number++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade {{ isset($processes) ? 'show active' : '' }}" id="nav-perhitungan" role="tabpanel" aria-labelledby="nav-perhitungan-tab">
                        <div class="p-3">
                            <form method="post" action="{{ route('hitung') }}">
                                {{ csrf_field() }}
                                
                                <!-- <div class="row">
                                    <div class="col-sm-3">
                                        <label for="alpha">Alpha</label>
                                        <select name="alpha" class="form-control form-control-sm" id="alpha">
                                            <option value="0.1">0.1</option>
                                            <option value="0.2">0.2</option>
                                            <option value="0.3">0.3</option>
                                            <option value="0.4">0.4</option>
                                            <option value="0.5">0.5</option>
                                            <option value="0.6">0.6</option>
                                            <option value="0.7">0.7</option>
                                            <option value="0.8">0.8</option>
                                            <option value="0.9">0.9</option>
                                        </select>
                                    </div>
                                </div> -->

                                <div class="row mt-3">
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-primary">Hitung</button>
                                    </div>
                                </div>
                            </form>

                            @if (isset($processes))

                                @foreach($processes as $process)                                    
                                    
                                    <h4 class="mt-4">Alpha {{ $process->result[0]['alpha'] }}</h4>

                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Bulan</th>
                                                <th scope="col">Tahun</th>
                                                <th scope="col">Jumlah</th>
                                                <th scope="col">St'</th>
                                                <th scope="col">St''</th>
                                                <th scope="col">At</th>
                                                <th scope="col">Bt</th>
                                                <th scope="col">Hasil Prediksi</th>
                                                <th scope="col">MAPE</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php $number = 1; @endphp
                                            @foreach($process->result as $result)
                                                <tr>
                                                    <td>{{ $number }}</td>
                                                    <td>{{ $result['nama_bulan'] }}</td>
                                                    <td>{{ $result['tahun'] }}</td>
                                                    <td>{{ $result['jumlah'] }}</td>
                                                    <td>{{ $result['st1'] }}</td>
                                                    <td>{{ $result['st2'] }}</td>
                                                    <td>{{ $result['at'] }}</td>
                                                    <td>{{ $result['bt'] }}</td>
                                                    <td>{{ $result['yt'] }}</td>
                                                    <td>{{ $result['mape'] }} % </td>
                                                    
                                                </tr>
                                                @php $number++; @endphp
                                            @endforeach
                                        </tbody>
                                        
                                        <tfoot>                                            
                                            @foreach($process->forecast as $forecast)
                                            <tr>
                                                <th scope="col"> Prediksi </th>
                                                <th scope="col"> {{ $forecast['bulan'] }} </th>
                                                <th scope="col"> {{ $forecast['tahun'] }} </th>
                                                <td>{{ $forecast['prediksi'] }} </td>
                                            
                                            </tr>
                                            @endforeach
                                        </tfoot>
                                    </table>
                                @endforeach

                                <div class="row mt-5">
                                    <div class="col-12">
                                        <canvas id="mixed-chart" height="100"></canvas>
                                        <div class="mr-5" id="mixed-chart-legend"></div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
    {!! Html::script('/assets/plugins/chartjs/chart.min.js') !!}
    {!! Html::script('/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
    {!! Html::script('/assets/js/dashboard.js') !!}
    
    <script>
        @php 
            $label = '';
            $labelArray = [];
            $dataAktual = '';
            $dataAktualArray = [];
            $total = [0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6 => '', 7 => '', 8 => ''];
            
            if (isset($charts)) {
                foreach($charts['label'] as $label) {
                    $labelArray[] = '"' . $label . '"';
                }
            
                $label = implode(',', $labelArray);

                foreach($charts['data_aktual'] as $dataAktual) {
                    $dataAktualArray[] = '"' . $dataAktual['jumlah'] . '"';
                }
            
                $dataAktual = implode(',', $dataAktualArray);

                foreach($charts['data_total'] as $k => $v) {
                    $total[$k] = $v;
                }
            }
            
        @endphp

        $(function() {
        "use strict";
        if ($("#mixed-chart").length) {
            var chartData = {
                labels: [{!! $label !!}],
                datasets: [
                    {
                        type: "line",
                        label: "Jumlah Data Aktual",
                        data: [{!! $dataAktual !!}],
                        backgroundColor: ChartColor[1],
                        borderColor: ChartColor[2],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.1",
                        data: [{!! $total[0] !!}],
                        backgroundColor: ChartColor[2],
                        borderColor: ChartColor[1],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.2",
                        data: [{!! $total[1] !!}],
                        backgroundColor: ChartColor[3],
                        borderColor: ChartColor[1],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.3",
                        data: [{!! $total[2] !!}],
                        backgroundColor: ChartColor[4],
                        borderColor: ChartColor[2],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.4",
                        data: [{!! $total[3] !!}],
                        backgroundColor: ChartColor[1],
                        borderColor: ChartColor[3],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.5",
                        data: [{!! $total[4] !!}],
                        backgroundColor: ChartColor[2],
                        borderColor: ChartColor[3],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.6",
                        data: [{!! $total[5] !!}],
                        backgroundColor: ChartColor[3],
                        borderColor: ChartColor[2],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.7",
                        data: [{!! $total[6] !!}],
                        backgroundColor: ChartColor[4],
                        borderColor: ChartColor[1],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.8",
                        data: [{!! $total[7] !!}],
                        backgroundColor: ChartColor[5],
                        borderColor: ChartColor[2],
                        borderWidth: 3,
                        fill: false
                    },
                    {
                        type: "line",
                        label: "Alpha 0.9",
                        data: [{!! $total[8] !!}],
                        backgroundColor: ChartColor[3],
                        borderColor: ChartColor[5],
                        borderWidth: 3,
                        fill: false
                    },
                ]
            };

            var MixedChartCanvas = document
                .getElementById("mixed-chart")
                .getContext("2d");

            var lineChart = new Chart(MixedChartCanvas, {
                type: "bar",
                data: chartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: "",
                        fontColor: chartFontcolor
                    },
                    scales: {
                        xAxes: [
                            {
                                display: true,
                                ticks: {
                                    fontColor: chartFontcolor,
                                    stepSize: 50,
                                    min: 0,
                                    max: 150,
                                    autoSkip: true,
                                    autoSkipPadding: 15,
                                    maxRotation: 0,
                                    maxTicksLimit: 10
                                },
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                    color: chartGridLineColor,
                                    zeroLineColor: chartGridLineColor
                                }
                            }
                        ],
                        yAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: "Total",
                                    fontSize: 12,
                                    lineHeight: 2,
                                    fontColor: chartFontcolor
                                },
                                ticks: {
                                    fontColor: chartFontcolor,
                                    display: true,
                                    autoSkip: false,
                                    maxRotation: 0,
                                    stepSize: 20000,
                                    min: 500,
                                    max: 280000
                                },
                                gridLines: {
                                    drawBorder: false,
                                    color: chartGridLineColor,
                                    zeroLineColor: chartGridLineColor
                                }
                            }
                        ]
                    },
                    legend: {
                        display: false
                    },
                    legendCallback: function(chart) {
                        var text = [];
                        text.push(
                            '<div class="chartjs-legend d-flex justify-content-center mt-4"><ul>'
                        );
                        for (var i = 0; i < chart.data.datasets.length; i++) {
                            console.log(chart.data.datasets[i]); // see what's inside the obj.
                            text.push("<li>");
                            text.push(
                                '<span style="background-color:' +
                                    chart.data.datasets[i].borderColor +
                                    '">' +
                                    "</span>"
                            );
                            text.push(chart.data.datasets[i].label);
                            text.push("</li>");
                        }
                        text.push("</ul></div>");
                        return text.join("");
                    }
                }
            });

            document.getElementById(
                "mixed-chart-legend"
            ).innerHTML = lineChart.generateLegend();
            
        }
    });
    </script>
@endpush
