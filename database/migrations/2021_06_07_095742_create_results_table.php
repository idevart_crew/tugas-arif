<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('bulan')->nullable();
            $table->string('nama_bulan', 50)->nullable();
            $table->integer('tahun')->nullable();
            $table->integer('jumlah')->nullable()->default(0);
            $table->integer('st1')->nullable()->default(0);
            $table->integer('st2')->nullable()->default(0);
            $table->float('at', 8, 2)->nullable()->default(0);
            $table->float('bt', 8, 2)->nullable()->default(0);
            $table->float('yt', 8, 2)->nullable()->default(0);
            $table->float('mape', 8, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
