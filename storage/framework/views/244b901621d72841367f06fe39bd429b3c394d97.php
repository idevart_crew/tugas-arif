<!DOCTYPE html>
<html>
<head>
  <title>Star Admin Pro Laravel Dashboard Template</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="<?php echo e(csrf_token()); ?>">
  
  <link rel="shortcut icon" href="<?php echo e(asset('/favicon.ico')); ?>">

  <!-- plugin css -->
  <?php echo Html::style('assets/plugins/@mdi/font/css/materialdesignicons.min.css'); ?>

  <?php echo Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css'); ?>

  <!-- end plugin css -->

  <?php echo $__env->yieldPushContent('plugin-styles'); ?>

  <!-- common css -->
  <?php echo Html::style('css/app.css'); ?>

  <!-- end common css -->

  <?php echo $__env->yieldPushContent('style'); ?>
</head>
<body data-base-url="<?php echo e(url('/')); ?>">

  <div class="container-scroller" id="app">
    <?php echo $__env->make('layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container-fluid page-body-wrapper">
      <?php echo $__env->make('layout.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="main-panel">
        <div class="content-wrapper">

          <?php if(session()->has('successMessage')): ?>
          <div class="toast" id="toast-sukses" style="position: absolute; top: 1; right: 20px; z-index: 9999" data-delay="3000">
            <div class="bg-success toast-body text-light">
                <strong>Sukses</strong><br/>
                <?php echo e(session()->get('successMessage')); ?>

            </div>
          </div>
          <?php endif; ?>

          <?php if(session()->has('errorMessage')): ?>
          <div class="toast" id="toast-gagal" style="position: absolute; top: 1; right: 20px; z-index: 9999" data-delay="3000">
            <div class="bg-danger toast-body text-light">
                <strong>Kesalahan</strong><br/>
                <?php echo e(session()->get('errorMessage')); ?>

            </div>
          </div>
          <?php endif; ?>
          
          <?php echo $__env->yieldContent('content'); ?>

        </div>
        <?php echo $__env->make('layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    </div>
  </div>

  <!-- base js -->
  <?php echo Html::script('js/app.js'); ?>

  <?php echo Html::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js'); ?>

  <!-- end base js -->

  <!-- plugin js -->
  <?php echo $__env->yieldPushContent('plugin-scripts'); ?>
  <!-- end plugin js -->

  <!-- common js -->
  <?php echo Html::script('assets/js/off-canvas.js'); ?>

  <?php echo Html::script('assets/js/hoverable-collapse.js'); ?>

  <?php echo Html::script('assets/js/misc.js'); ?>

  <?php echo Html::script('assets/js/settings.js'); ?>

  <?php echo Html::script('assets/js/todolist.js'); ?>

  <!-- end common js -->

  <?php echo $__env->yieldPushContent('custom-scripts'); ?>
  <script>
    $(document).ready(function(){
      <?php if(session()->has('successMessage')): ?>
        $('#toast-sukses').toast('show');
      <?php endif; ?>

      <?php if(session()->has('errorMessage')): ?>
        $('#toast-gagal').toast('show');
      <?php endif; ?>
    });
  </script>
</body>
</html>